all: add-nbo

add-nbo: main.cpp
	g++ -o add-nbo main.cpp

clean:
	rm -f *.o
	rm -f *.bin
	rm -f add-nbo
